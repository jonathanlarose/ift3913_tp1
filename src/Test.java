/**
 * Created by Jonathan on 2017-11-01.
 */
public class Test {

    public static void main(String[] args) {

        //Test calcul des métriques dans un temps raisonnable
        Model model = new Model("test");
        int nbClasse = 25000;

        for (int i = 0; i < nbClasse; i++) {

            Classe c = new Classe("c" + i);

            for (int j = 0; j < 10; j++) {

                Operation o = new Operation("o" + i + j, "String");

                for (int k=0; k < 3; k++) {
                    if(k%2 == 0)
                        o.addArgument("a" + i + j + k, "c" + (i-1));
                    else
                        o.addArgument("a" + i + j + k, "String");
                }

                c.addOp(o);
            }

            model.add(c);
        }

        for(int i=1; i<nbClasse/100; i++){

            Classe c = model.findClasse("c" + (i*100));
            Generalization g = new Generalization(c);

            for(int j=1; j<50; j++){
                Classe sc  = model.findClasse("c" + (i*100 - j));
                g.addSubclass(sc);

            }

            model.add(g);
        }

        System.out.println("MODEL GÉGÉRÉ");

        Classe c = model.findClasse("c1200");

        System.out.println("ANA = " + Metriques.ana(c));
        System.out.println("NOM = " + Metriques.nom(model,c));
        System.out.println("NOA = " + Metriques.noa(model,c));
        System.out.println("ITC = " + Metriques.itc(model,c));
        System.out.println("ETC = " + Metriques.etc(model,c));
        System.out.println("CAC = " + Metriques.cac(model,c));
        System.out.println("DIT = " + Metriques.dit(model,c));
        System.out.println("CLD = " + Metriques.cld(model,c));
        System.out.println("NOC = " + Metriques.noc(model,c));
        System.out.println("NOD = " + Metriques.nod(model,c));
    }
}
