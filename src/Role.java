/**
 * Développé par:
 * Augusto dos Santos Latgé 20083794
 * Jonathan Larose 20066082.
 */

public class Role {

    //Attributs
    private Classe classe;
    private Multiplicity multiplicity;

    //Getters
    public Classe getClasse() {
        return classe;
    }
    public Multiplicity getMultiplicity() {
        return multiplicity;
    }

    //Constructeur
    public Role(Classe classe, Multiplicity multiplicity){

        this.classe = classe;
        this.multiplicity = multiplicity;
    }

    /**
     * Fonction pour l'affichage
     * @return
     */
    public String toString(){

        return classe + " " + multiplicity;
    }
}
