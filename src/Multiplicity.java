/**
 * Développé par:
 * Augusto dos Santos Latgé 20083794
 * Jonathan Larose 20066082.
 */
public enum Multiplicity {
    ONE, ONE_OR_MANY, MANY, OPTIONALLY_ONE, UNDEFINED
}
