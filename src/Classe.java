import javax.swing.*;
import java.util.ArrayList;

/**
 * Développé par:
 * Augusto dos Santos Latgé 20083794
 * Jonathan Larose 20066082.
 */

public class Classe {

    //Attributs
    private String id;  //Identificateur de la classe
    private ArrayList<String[]> attributList;   //Liste d'attibute de la classe
    private ArrayList<Operation> operationsList; //Liste d'opérations de la classe
    private DefaultListModel listeMetriques =  null; //Liste des métriques de la classe (initialisée à "null")

    //Setter
    public void setListeMetriques(DefaultListModel listeMetriques) {this.listeMetriques = listeMetriques;}

    //Getters
    public ArrayList<String[]> getAttributList() {
        return attributList;
    }
    public ArrayList<Operation> getOperationsList() {
        return operationsList;
    }
    public String getId() {
        return id;
    }
    public DefaultListModel getListeMetriques() {return listeMetriques;}

    //Constructeur
    public Classe(String id){

        this.id = id;
        //Initialise les ArrayList à 0. Des objets s'ajouteront au liste au cours du parsing
        this.attributList = new ArrayList<String[]>(0);
        this.operationsList = new ArrayList<Operation>(0);
    }

    /**
     * Fonction pour ajouter des attributs à la liste d'attribut de la classe
     * @param att Tableau de String représentant l'attribut à ajouter à la liste d'attribut [[id],[type]]
     */
    public void addAtt(String[] att){

        for(String[] s : this.attributList){

            if(s[0].equals(att[0]))
                return;
        }
        this.attributList.add(att);
    }

    /**
     * Fonction pour ajouter des opérations à la liste d'opérations de la classe
     * @param op String attribut à ajouter à la liste d'attribut [[nom fonction et arguments],[type]]
     */
    public void addOp(Operation op){

        for(Operation o : this.operationsList){

            if(o.equals(op))
                return;
        }
        this.operationsList.add(op);
    }

    /**
     * Pour afficher directement le id
     * @return String id
     */
    public String toString(){
        return id;
    }
    /**
     * Fonction qui retourne un String représentant la Classe et ses attributs de façon détaillé
     * @return Un String représentant la classe
     */
    public String toStringDetail(){

        String str = "Classe : " + id + "\n";

        str += "Attributs :\n";
        for (String[] a : attributList) {
            str += "\t";
            for(int i=0; i<a.length; i++)
                str += a[i] + " ";
            str += "\n";
        }

        str += "Opérations :\n";
        for (Operation o : operationsList)
            str += "\t" + o + "\n";

        return str;
    }
}
