import java.util.ArrayList;

/**
 * Développé par:
 * Augusto dos Santos Latgé 20083794
 * Jonathan Larose 20066082.
 */

public class Aggregation implements Link{

    //Attributs
    private Role roleContainer;
    private ArrayList<Role> rolePartList;

    //Getters
    public Role getRoleContainer() {
        return roleContainer;
    }
    public ArrayList<Role> getRolePartList() {
        return rolePartList;
    }

    //Constructeur
    public Aggregation(Role roleContainer){

        this.roleContainer = roleContainer;
        this.rolePartList = new ArrayList<Role>(0);
    }

    /**
     * Fonction qui ajoute des roles faisant partie de la classe dans le role Container
     * @param r role à ajouter
     */
    public void addRole(Role r){

        this.rolePartList.add(r);
    }

    /**
     * Fonction qui vérifie si la classe passé en argument fait partie de la Relation / Aggregation
     * @param c Classe qu'on veut vérifier
     * @return True si oui, false si non
     */
    public boolean isClasseInLink(Classe c){

        if(roleContainer.getClasse().equals(c))
            return true;

        else{

            for(Role p : rolePartList){

                if(p.getClasse().equals(c))
                    return true;
            }
        }

        return false;
    }

    /**
     * Fonction qui retourne un string représentant les détails Relation / Aggregation
     * @return String détails de la Relation / Aggregation
     */
    public String toString(){

        String str = "AGGREGATION\n";
        str += "        " + "CONTAINER\n";
        str += "                " + "CLASS " + roleContainer + "\n";
        str += "        " + "PARTS\n";

        for(Role r : rolePartList)
            str += "                " + "CLASS " + r + "\n";

        return str;
    }

    /**
     * Fonction qui retourne un string représentant la Relation / Aggregation en fonction de la classe passé en argument
     * @param c Classe
     * @return String de la Relation / Aggregation relativement à la classe
     */
    public String toStringRelative(Classe c){

        String str = "(A) ";

        if(!c.equals(roleContainer.getClasse()))
            str += "C_" + roleContainer.getClasse();
        else{
            for(Role r : rolePartList)
                str += "P_" + r.getClasse() + ", ";

            str = str.substring(0,str.length()-2);
        }

        return str;
    }
}
