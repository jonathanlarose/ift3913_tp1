import java.util.ArrayList;

/**
 * Created by Jonathan on 2017-10-30.
 */
public class Operation {

    //Attributs
    private String id;
    private String returnType;
    private ArrayList<String[]> arguments;

    //Getters
    public String getId() {
        return id;
    }

    public String getReturnType(){
        return returnType;
    }

    public ArrayList<String[]> getArguments() {
        return arguments;
    }

    //Constructeur
    public Operation(String id, String returnType){

        this.id = id;
        this.returnType = returnType;
        this.arguments = new ArrayList<String[]>(0);
    }

    public void addArgument(String argId, String type){

        this.arguments.add(new String[] {argId, type});
    }

    /**
     * Fonction qui retourne une liste de tous les types d'argument en entré à la méthode
     * @return Arraylist des types
     */
    public ArrayList<String> getArgTypeList(){

        ArrayList<String> typeList = new ArrayList<String>(0);

        for(String[] arg : this.arguments){
            typeList.add(arg[1]);
        }

        return typeList;
    }

    public String toString(){

        String str = (returnType.equals("void"))? "" : returnType + " ";
        str += this.id + "(";

        for(String[] s : arguments)
            str += s[0] + " : " + s[1] + ", ";

        //Retire la dernière virgule inutile et ferme la parenthèse
        if(!this.arguments.isEmpty())
            str = str.substring(0, str.length()-2);

        str += ")";

        return str;
    }

    /**
     * Fonction qui compare l'opération et retourne true si elle sont identique (redefini ailleur)
     * @param o Opération avec laquelle on veut comparer
     * @return true redefinition, false si ce sont deux opérations distincte
     */
    public Boolean equals(Operation o){

        if(!id.equals(o.getId()))
            return false;
        if(!returnType.equals(o.getReturnType()))
            return false;
        if(arguments.size() != o.getArguments().size())
            return false;

        for(int i=0; i<arguments.size(); i++){

            String[] arg1 = arguments.get(i);
            String[] arg2 = o.getArguments().get(i);

            if(!arg1[0].equals(arg2[0]) || !arg1[1].equals(arg2[1]))
                return false;
        }


        return true;
    }
}
