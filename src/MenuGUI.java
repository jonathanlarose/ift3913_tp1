import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 * Développé par:
 * Augusto dos Santos Latgé 20083794
 * Jonathan Larose 20066082.
 */

/**
 * La classe MenuGUI est responsable par l'interface graphique avec l'utilisateur
 */
public class MenuGUI extends JFrame {

    private Model model;                // modèle parsé fourni par le parseur
    private Classe classeActive;        // garde la classe selectionnée dans l'interface
    private File fileF;                  // fichier qui contient le model à parser

    //Les élements graphiques du Borderlayout utilisé pour l'affichage divisés par les zones d'affichage

    private JPanel principal;

    //North
    private JPanel north;

    //NorthWest
    private JButton butChargerFichier;

    //NorthCenter
    private JTextField tFChargerFichier;

    //NorthEast
    private JButton butCalculMetriques;

    //West
    private JPanel west;
    private JLabel labClasses;
    private JList jLClasses;
    private JScrollPane sPjLClasses;
    private JPanel zoneButton;
    private JButton butGenererCSV;

    //Center
    private JPanel center;

    //CenterTop
    private JPanel centerTop;

    //CenterTopWest
    private JPanel centerTopWest;
    private JLabel labAttributs;
    private JTextArea tAAttributs;
    private JScrollPane sPtAAttributs;

    //CenterTopEast
    private JPanel centerTopEast;
    private JLabel labMethodes;
    private JTextArea tAMethodes;
    private JScrollPane sPtAMethodes;

    //CenterMiddle
    private JPanel centerMiddle;

    //CenterMiddleWest
    private JPanel centerMiddleWest;
    private JLabel labSousClasses;
    private JTextArea tASousClasses;
    private JScrollPane sPtASousClasses;


    //CenterMiddleEast
    private JPanel centerMiddleEast;
    private JLabel labAssAgr;
    private JList jLAssAgr;
    private JScrollPane sPjLAssAgr;

    //CenterBottom
    private JPanel centerBottom;
    private JLabel labDetails;
    private JTextArea tADetails;
    private JScrollPane sPtADetails;

    //East
    private JPanel east;
    private JLabel labMetriques;
    private JList jLMetriques;
    private JScrollPane sPjLMetriques;

    /**
     * Constructeur MenuGUI
     * Responsable pour l'affichage des éléments de l'interface graphique
     */
    public MenuGUI() {
        super("Afficheur UML");
        setSize(1000, 800);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        //JPanel principal
        principal = new JPanel();

        //North
        north = new JPanel();
        butChargerFichier = new JButton("Charger fichier");
        tFChargerFichier = new JTextField(50);
        butCalculMetriques = new JButton ("Calculer métriques");

        //West
        west = new JPanel();
        labClasses = new JLabel("Classes");
        jLClasses = new JList();
        sPjLClasses = new JScrollPane(
            jLClasses,
            JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
            JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        zoneButton = new JPanel();
        butGenererCSV = new JButton("Générer métriques (CSV)");

        //Center
        center = new JPanel();

        //CenterTop
        centerTop = new JPanel();

        //CenterTopWest
        centerTopWest = new JPanel();
        labAttributs = new JLabel("Attributs");
        tAAttributs = new JTextArea(15, 15);
        sPtAAttributs = new JScrollPane(
            tAAttributs,
            JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
            JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        //CenterTopEast
        centerTopEast = new JPanel();
        labMethodes = new JLabel("Méthodes");
        tAMethodes = new JTextArea(15, 20);
        sPtAMethodes = new JScrollPane(
            tAMethodes,
            JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
            JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        //CenterMiddle
        centerMiddle = new JPanel();

        //CenterMiddleWest
        centerMiddleWest = new JPanel();
        labSousClasses = new JLabel("Sous-classes");
        tASousClasses = new JTextArea(10, 20);
        sPtASousClasses = new JScrollPane(
            tASousClasses,
            JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
            JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);


        //CenterMiddleEast
        centerMiddleEast = new JPanel();
        labAssAgr = new JLabel("Associations/agrégations");
        jLAssAgr = new JList();
        sPjLAssAgr = new JScrollPane(
            jLAssAgr,
            JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
            JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        //CenterBottom
        centerBottom = new JPanel();
        labDetails = new JLabel("Détails");
        tADetails = new JTextArea(10, 10);
        sPtADetails = new JScrollPane(
            tADetails,
            JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
            JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        //East
        east = new JPanel();
        labMetriques = new JLabel("Métriques");
        jLMetriques = new JList();
        sPjLMetriques = new JScrollPane(
                jLMetriques,
                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        //Marges
        principal.setBorder(BorderFactory.createEmptyBorder(15, 15, 15, 15));

        BorderLayout borderLayoutPrincipal = new BorderLayout();
        borderLayoutPrincipal.setHgap(15);                          //Padding
        borderLayoutPrincipal.setVgap(15);
        principal.setLayout(borderLayoutPrincipal);


        //North
        BorderLayout borderLayoutNorth = new BorderLayout();
        borderLayoutNorth.setHgap(15);
        north.setLayout(borderLayoutNorth);
        north.add(butChargerFichier, BorderLayout.WEST);
        north.add(tFChargerFichier, BorderLayout.CENTER);
        north.add(butCalculMetriques, BorderLayout.EAST);
        principal.add(north, BorderLayout.NORTH);

        //West
        west.setLayout(new BorderLayout());
        west.setPreferredSize(new Dimension(220, 10));          //garder la dimension horizontal de panneaux
        jLClasses.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);   //selection d'un item de la liste à la fois
        west.add(labClasses, BorderLayout.NORTH);
        west.add(sPjLClasses, BorderLayout.CENTER);
        zoneButton.setBorder(BorderFactory.createEmptyBorder(15, 0,0,0));
        zoneButton.add(butGenererCSV);
        west.add(zoneButton, BorderLayout.SOUTH);
        principal.add(west, BorderLayout.WEST);

        BorderLayout borderLayoutCenter = new BorderLayout();
        borderLayoutCenter.setVgap(15);

        BorderLayout borderLayoutCenterMiddle = new BorderLayout();
        borderLayoutCenterMiddle.setHgap(15);

        BorderLayout borderLayoutCenterTop = new BorderLayout();
        borderLayoutCenterTop.setHgap(15);

        //CenterTopWest
        centerTopWest.setLayout(new BorderLayout());
        centerTopWest.setPreferredSize(new Dimension(220, 10)); //garder la dimension horizontal de panneaux
        tAAttributs.setEditable(false);                                    //les fenêtres de text ne sont pas editables
        centerTopWest.add(labAttributs, BorderLayout.NORTH);
        centerTopWest.add(sPtAAttributs, BorderLayout.CENTER);

        //CenterTopEast
        centerTopEast.setLayout(new BorderLayout());
        tAMethodes.setEditable(false);                                     //les fenêtres de text ne sont pas editables
        centerTopEast.add(labMethodes, BorderLayout.NORTH);
        centerTopEast.add(sPtAMethodes, BorderLayout.CENTER);

        //CenterTop
        centerTop.setLayout(borderLayoutCenterTop);
        centerTop.add(centerTopWest, BorderLayout.WEST);
        centerTop.add(centerTopEast, BorderLayout.CENTER);

        //CenterMiddleWest
        centerMiddleWest.setLayout(new BorderLayout());
        tASousClasses.setEditable(false);                                  //les fenêtres de text ne sont pas editables
        centerMiddleWest.add(labSousClasses, BorderLayout.NORTH);
        centerMiddleWest.add(sPtASousClasses, BorderLayout.CENTER);

        //CenterMiddleWest
        centerMiddleEast.setLayout(new BorderLayout());
        jLAssAgr.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);     //selection d'un item de la liste à la fois
        centerMiddleEast.add(labAssAgr, BorderLayout.NORTH);
        centerMiddleEast.add(sPjLAssAgr, BorderLayout.CENTER);

        //CenterMiddle
        centerMiddle.setLayout(borderLayoutCenterMiddle);
        centerMiddle.add(centerMiddleWest, BorderLayout.WEST);
        centerMiddle.add(centerMiddleEast, BorderLayout.CENTER);

        //CenterBottom
        centerBottom.setLayout(new BorderLayout());
        tADetails.setEditable(false);                               //les fenêtres de text ne sont pas editables
        centerBottom.add(labDetails, BorderLayout.NORTH);
        centerBottom.add(sPtADetails, BorderLayout.CENTER);

        //Center
        center.setLayout(borderLayoutCenter);
        center.add(centerTop, BorderLayout.NORTH);
        center.add(centerMiddle, BorderLayout.CENTER);
        center.add(centerBottom, BorderLayout.SOUTH);
        principal.add(center, BorderLayout.CENTER);

        //East
        east.setLayout(new BorderLayout());
        east.setPreferredSize(new Dimension(185, 10));          //garder la dimension horizontal de panneaux
        jLMetriques.setSelectionMode(ListSelectionModel.SINGLE_SELECTION); //selection d'un item de la liste à la fois
        east.add(labMetriques, BorderLayout.NORTH);
        east.add(sPjLMetriques, BorderLayout.CENTER);
        principal.add(east, BorderLayout.EAST);

        add(principal);

        setVisible(true);

        //Écouteur du button "Charger fichier" pour l'ouverture d'un fichier à parser
        butChargerFichier.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser directory = new JFileChooser();
                directory.setCurrentDirectory(new File("."));
                directory.showOpenDialog(principal); //open dialog
                fileF = directory.getSelectedFile();
                if (fileF != null) {                            //si un fichier à été choisi

                    String ext = fileF.getName().substring(fileF.getName().length()-3,fileF.getName().length());
                    if (ext.equals("ucd")) {                        //si la bonne extension ".ucd"
                        tFChargerFichier.setText(fileF.getName());  //afficher le nom du fichier dans le JTextField
                        afficheurChargerFile(fileF.getPath());      //appeller la méthode qui va gérer l'ouverture et le parsing
                        //du fichier .UCD
                    } else {
                        JOptionPane.showMessageDialog(null, "Sélectionnez un fichier du type \".ucd\" !");
                    }
                }
            }
        });

        //Écouteur du button "Calculer métriques" pour faire le calcul des métriques
        butCalculMetriques.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                //Vérifier s'il y a une classe déjà initialisé (diagramme à parser déjà chargé)
                if(classeActive != null) {
                    // méthode de calcul des métriques
                    afficheurMetriques(classeActive);
                }
            }
        });

        //Écouteur du button "Générer métriques (CSV)" pour faire le calcul et impression de la matrice des métriques
        butGenererCSV.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                //Vérifier s'il y a une classe déjà initialisé (diagramme à parser déjà chargé)
                if(model != null) {
                    
                    // nom du fichier à générer
                    String nomDuFichier = fileF.getName().substring(0,fileF.getName().length()-4)+"_metriques.csv";
                    
                    // méthode de calcul des métriques
                    String matriceMetriques = Metriques.calculMatriceCSV(model);
                    
                    // création du fichier .csv
                    genererMetriqueCSV(matriceMetriques,nomDuFichier);
                    
                    // affiche message de succès de la génération du fichier
                    JOptionPane.showMessageDialog(null, "Fichier \""+nomDuFichier+"\" généré avec succès!");
                }
            }
        });

        //Écouteur des cliques sur les items du JList des Classes
        jLClasses.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent event) {

                if (event.getValueIsAdjusting()) {

                    JList source = (JList) event.getSource();
                    int indexClasse = source.getSelectedIndex();            //index de l'item selectionné
                    ArrayList<Classe> classeList = model.getClasseList();   //cherche la classé en fonction de l'item choisi

                    jLClasses.setSelectedIndex(indexClasse);        //montre l'item selectionné
                    classeActive = classeList.get(indexClasse);     //dans la liste et défini la nouvelle classe active
                    afficheurClasse(classeActive);
                }
            }
        });

        //Écouteur des cliques sur les items du JList des Associations/Agrégations
        jLAssAgr.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent event) {

                if (event.getValueIsAdjusting()) {

                    JList source = (JList) event.getSource();
                    int indexClasse = source.getSelectedIndex();

                    jLAssAgr.setSelectedIndex(indexClasse);                             //index de l'item selectionné
                    ArrayList<Link> linkList = model.findClasseLink(classeActive);      //recupère les listes des relations

                    if(!linkList.isEmpty()) {                                           //s'il y a au moins une relation
                        tADetails.setText(linkList.get(indexClasse).toString());        //affiche la relation
                    }

                    //Effacer la selection des métriques dans JList Métriques
                    if(classeActive.getListeMetriques() != null){
                        jLMetriques.setModel(classeActive.getListeMetriques());
                    } else {
                        DefaultListModel emptyList = new DefaultListModel();
                        emptyList.clear();
                        jLMetriques.setModel(emptyList);
                    }
                }
            }
        });

        //Écouteur des cliques sur les items du JList des Métriques
        jLMetriques.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent event) {
                if (event.getValueIsAdjusting()) {

                    JList source = (JList) event.getSource();
                    int indexClasse = source.getSelectedIndex();

                    //Effacer la selection des Associations/Agrégations
                    //Mais garder l'affichage des éléments dans JList Associations/Agrégations
                    DefaultListModel listeModelAssAgr = new DefaultListModel();
                    ArrayList<Link> linkList = model.findClasseLink(classeActive);

                    if (!linkList.isEmpty()) {                       // s'il y a des Associations et/ou Agrégations
                        for (Link link : linkList) {
                            listeModelAssAgr.addElement(link.toStringRelative(classeActive));
                        }
                        jLAssAgr.setModel(listeModelAssAgr);            // affiche la liste des Associations et/ou Agrégations
                    }

                    // Affichage fenêtres Détails
                    tADetails.setText(Metriques.definition(source.getSelectedValue().toString().substring(0,3)));
                }
            }
        });

    }

    /**
     * Méthode qui fait le appel à la fonction de parsing et d'affichage des paramètres des classes
     * @param file : path du file
     */
    private void afficheurChargerFile(String file) {

        model = Parser.parse(file);

        if (model != null) {                    //Vérifie si fichier vide
            // Affichage fenêtre Classes
            DefaultListModel listeModelClasses = new DefaultListModel();
            ArrayList<Classe> classeList = model.getClasseList();
            for (Classe classe : classeList) {
                listeModelClasses.addElement(classe.getId());
            }
            jLClasses.setModel(listeModelClasses);

            // Si le fichier charge possède au moins une classe
            if (!classeList.isEmpty()) {
                classeActive = classeList.get(0);
                afficheurClasse(classeActive);
            }

            jLClasses.setSelectedIndex(0);
        }

    }

    /**
     * Méthode appelé lors de la selection d'une nouvelle classe dans la fenêtre Classes (JList)
     * @param classe : classe à être affiché
     */
    private void afficheurClasse(Classe classe) {

        // Affichage fenêtre Attributs
        ArrayList<String[]> attributList = classe.getAttributList();
        String attributsS = "";
        for (String[] elementAttr : attributList) {
            attributsS += elementAttr[1].toString() + " " + elementAttr[0].toString() + "\n";
        }
        tAAttributs.setText(attributsS);    //affiche les attributs existants (rien s'inexistants)


        // Affichage fenêtre Méthodes
        ArrayList<Operation> methodesList = classe.getOperationsList();
        String methodeS = "";
        for (Operation elementMeth : methodesList) {

            methodeS += elementMeth + "\n";
        }
        tAMethodes.setText(methodeS);       //affiche les méthodes existantes (rien s'inexistantes)

        // Affichage fenêtre Sous-classes
        Generalization gener = model.findClasseGeneralization(classeActive);
        if(gener != null) {
            ArrayList<Classe> generList = gener.getSubclasses();
            String generS = "";
            for (Classe elementGener : generList) {
                generS += elementGener.toString() + "\n";
            }
            tASousClasses.setText(generS);  //affiche les sous-classes existantes
        } else {
            tASousClasses.setText("");      //n'affiche rien s'il n'y a pas de sous-classes
        }

        // Affichage fenêtres Associations/Agrégations et Détails
        DefaultListModel listeModelAssAgr = new DefaultListModel();
        ArrayList<Link> linkList = model.findClasseLink(classeActive);

        if(!linkList.isEmpty()) {                       // s'il y a des Associations et/ou Agrégations
            for (Link link : linkList) {
                listeModelAssAgr.addElement(link.toStringRelative(classeActive));
            }
            jLAssAgr.setModel(listeModelAssAgr);            // affiche la liste des Associations et/ou Agrégations
            tADetails.setText(linkList.get(0).toString());  // affiche les détails correspondants
            jLAssAgr.setSelectedIndex(0);

        } else {                                        // s'il n'y a pas des Associations ou Agrégations
            listeModelAssAgr.clear();                   // vide la liste des Associations et/ou Agrégations
            tADetails.setText("");                      // vide la fenêtre de Détails
            jLAssAgr.setModel(listeModelAssAgr);
        }

        // Afficher les métriques si elles sont déjà calculées
        if(classe.getListeMetriques() != null){
            jLMetriques.setModel(classe.getListeMetriques());
        } else {
            DefaultListModel emptyList = new DefaultListModel();
            emptyList.clear();
            jLMetriques.setModel(emptyList);
        }
    }

    /**
     * Méthode appelé lors de l'apui sur le buttom "Calculer métriques"
     * @param classe : classe à être affiché
     */
    private void afficheurMetriques(Classe classe) {

        if(classe.getListeMetriques() == null) {                            //Calculer le métriques une seule fois
            DefaultListModel listeMetriques = new DefaultListModel();

            //Appel du calcul des métriques et affichage dans JList Métriques
            listeMetriques.addElement("ANA = "+(double) Math.round(Metriques.ana(classeActive)*100)/100);
            listeMetriques.addElement("NOM = "+Metriques.nom(model, classeActive));
            listeMetriques.addElement("NOA = "+Metriques.noa(model, classeActive));
            listeMetriques.addElement("ITC = "+Metriques.itc(model, classeActive));
            listeMetriques.addElement("ETC = "+Metriques.etc(model, classeActive));
            listeMetriques.addElement("CAC = "+Metriques.cac(model, classeActive));
            listeMetriques.addElement("DIT = "+Metriques.dit(model, classeActive));
            listeMetriques.addElement("CLD = "+Metriques.cld(model, classeActive));
            listeMetriques.addElement("NOC = "+Metriques.noc(model, classeActive));
            listeMetriques.addElement("NOD = "+Metriques.nod(model, classeActive));

            classe.setListeMetriques(listeMetriques);
            jLMetriques.setModel(listeMetriques);               //Affichage dans JList Métriques

        } else {
            jLMetriques.setModel(classe.getListeMetriques());   //Affichage dans JList Métriques
        }
    }

    /**
     * Fonction pour la génération du fichier .csv
     * @param matCSV
     * @param nomDuFichier
     */
    public static void genererMetriqueCSV(String matCSV, String nomDuFichier) {

        BufferedWriter bw = null;
        FileWriter fw = null;

        try {
            fw = new FileWriter(nomDuFichier);
            bw = new BufferedWriter(fw);
            bw.write(matCSV);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bw != null)
                    bw.close();
                if (fw != null)
                    fw.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
