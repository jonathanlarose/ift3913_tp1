import java.util.ArrayList;

/**
 * Développé par:
 * Augusto dos Santos Latgé 20083794
 * Jonathan Larose 20066082.
 */

public class Generalization {

    //Attributs
    private String id;
    private ArrayList<String> subclasses;

    //Getters
    public String getId() {
        return id;
    }
    public ArrayList<String> getSubclasses() {
        return subclasses;
    }

    public Generalization(String id){
        this.id = id;
        this.subclasses = new ArrayList<String>(0);
    }

    /**
     * Fonction qui ajoute une sous-classes à la liste de sous-classes
     * @param subclass String Identificateur de la sous-classes à ajouter
     */
    public void addSubclass(String subclass){
        this.subclasses.add(subclass);
    }
}
