import java.util.ArrayList;

/**
 * Développé par:
 * Augusto dos Santos Latgé 20083794
 * Jonathan Larose 20066082.
 */

public class Relation implements Link{

    //Attributs
    private String id;
    private Role role1;
    private Role role2;

    //Getters
    public String getId() {
        return id;
    }
    public Role getRole1() {
        return role1;
    }
    public Role getRole2() {
        return role2;
    }

    //Setters
    public void setRole1(Role role1) {
        this.role1 = role1;
    }
    public void setRole2(Role role2) {
        this.role2 = role2;
    }

    //Constructeur
    public Relation(String id){

        this.id = id;
    }

    /**
     * Fonction qui vérifie si la classe passé en argument fait partie de la Relation / Aggregation
     * @param c Classe qu'on veut vérifier
     * @return True si oui, false si non
     */
    public boolean isClasseInLink(Classe c){

        String classId = c.getId();

            if(role1.getId().equals(classId) || role2.getId().equals(classId))
                return true;

        return false;
    }

    /**
     * Fonction qui retourne un string représentant les détails Relation / Aggregation
     * @return String détails de la Relation / Aggregation
     */
    public String toString(){

        String str = "RELATION " + id + "\n";
        str += "        " + "ROLES\n";
        str += "                " + "CLASS " + role1 + "\n";
        str += "                " + "CLASS " + role2 + "\n";

        return str;
    }

    /**
     * Fonction qui retourne un string représentant la Relation / Aggregation en fonction de la classe passé en argument
     * @param c Classe
     * @return String de la Relation / Aggregation relativement à la classe
     */
    public String toStringRelative(Classe c){

        String str = "(R) ";

        if(c.getId().equals(role2.getId()))
            str += "inv_";

        str += id;

        return str;
    }
}
