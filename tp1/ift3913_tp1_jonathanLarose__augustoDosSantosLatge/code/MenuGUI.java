import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;

/**
 * Développé par:
 * Augusto dos Santos Latgé 20083794
 * Jonathan Larose 20066082.
 */

/**
 * La classe MenuGUI est responsable par l'interface graphique avec l'utilisateur
 */
public class MenuGUI extends JFrame {

    private Model model;                // modèle parsé fourni par le parseur
    private Classe classeActive;        // garde la classe selectionnée dans l'interface

    //Les élements graphiques du Borderlayout utilisé pour l'affichage divisés par les zones d'affichage

    private JPanel principal;

    //North
    private JPanel north;
    private JButton butChargerFichier;
    private JTextField tFChargerFichier;

    //West
    private JPanel west;
    private JLabel labClasses;
    private JList jLClasses;
    private JScrollPane sPjLClasses;

    //Center
    private JPanel center;

    //CenterTop
    private JPanel centerTop;

    //CenterTopWest
    private JPanel centerTopWest;
    private JLabel labAttributs;
    private JTextArea tAAttributs;
    private JScrollPane sPtAAttributs;

    //CenterTopEast
    private JPanel centerTopEast;
    private JLabel labMethodes;
    private JTextArea tAMethodes;
    private JScrollPane sPtAMethodes;

    //CenterMiddle
    private JPanel centerMiddle;

    //CenterMiddleWest
    private JPanel centerMiddleWest;
    private JLabel labSousClasses;
    private JTextArea tASousClasses;
    private JScrollPane sPtASousClasses;


    //CenterMiddleEast
    private JPanel centerMiddleEast;
    private JLabel labAssAgr;
    private JList jLAssAgr;
    private JScrollPane sPjLAssAgr;

    //CenterBottom
    private JPanel centerBottom;
    private JLabel labDetails;
    private JTextArea tADetails;
    private JScrollPane sPtADetails;

    /**
     * Constructeur MenuGUI
     * Responsable pour l'affichage des éléments de l'interface graphique
     */
    public MenuGUI() {
        super("Afficheur UML");
        setSize(800, 800);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        //JPanel principal
        principal = new JPanel();

        //North
        north = new JPanel();
        butChargerFichier = new JButton("Charger fichier");
        tFChargerFichier = new JTextField(50);

        //West
        west = new JPanel();
        labClasses = new JLabel("Classes");
        jLClasses = new JList();
        sPjLClasses = new JScrollPane(
            jLClasses,
            JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
            JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        //Center
        center = new JPanel();

        //CenterTop
        centerTop = new JPanel();

        //CenterTopWest
        centerTopWest = new JPanel();
        labAttributs = new JLabel("Attributs");
        tAAttributs = new JTextArea(15, 15);
        sPtAAttributs = new JScrollPane(
            tAAttributs,
            JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
            JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        //CenterTopEast
        centerTopEast = new JPanel();
        labMethodes = new JLabel("Méthodes");
        tAMethodes = new JTextArea(15, 20);
        sPtAMethodes = new JScrollPane(
            tAMethodes,
            JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
            JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        //CenterMiddle
        centerMiddle = new JPanel();

        //CenterMiddleWest
        centerMiddleWest = new JPanel();
        labSousClasses = new JLabel("Sous-classes");
        tASousClasses = new JTextArea(10, 20);
        sPtASousClasses = new JScrollPane(
            tASousClasses,
            JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
            JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);


        //CenterMiddleEast
        centerMiddleEast = new JPanel();
        labAssAgr = new JLabel("Associations/agrégations");
        jLAssAgr = new JList();
        sPjLAssAgr = new JScrollPane(
            jLAssAgr,
            JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
            JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        //CenterBottom
        centerBottom = new JPanel();
        labDetails = new JLabel("Détails");
        tADetails = new JTextArea(10, 10);
        sPtADetails = new JScrollPane(
            tADetails,
            JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
            JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        //Marges
        principal.setBorder(BorderFactory.createEmptyBorder(15, 15, 15, 15));

        BorderLayout borderLayoutPrincipal = new BorderLayout();
        borderLayoutPrincipal.setHgap(15);                          //Padding
        borderLayoutPrincipal.setVgap(15);
        principal.setLayout(borderLayoutPrincipal);


        //North
        north.add(butChargerFichier);
        north.add(tFChargerFichier);
        principal.add(north, BorderLayout.NORTH);

        //West
        west.setLayout(new BorderLayout());
        west.setPreferredSize(new Dimension(220, 10));          //garder la dimension horizontal de panneaux
        jLClasses.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);   //selection d'un item de la liste à la fois
        west.add(labClasses, BorderLayout.NORTH);
        west.add(sPjLClasses, BorderLayout.CENTER);
        principal.add(west, BorderLayout.WEST);

        BorderLayout borderLayoutCenter = new BorderLayout();
        borderLayoutCenter.setVgap(15);

        BorderLayout borderLayoutCenterMiddle = new BorderLayout();
        borderLayoutCenterMiddle.setHgap(15);

        BorderLayout borderLayoutCenterTop = new BorderLayout();
        borderLayoutCenterTop.setHgap(15);

        //CenterTopWest
        centerTopWest.setLayout(new BorderLayout());
        centerTopWest.setPreferredSize(new Dimension(220, 10)); //garder la dimension horizontal de panneaux
        tAAttributs.setEditable(false);                                    //les fenêtres de text ne sont pas editables
        centerTopWest.add(labAttributs, BorderLayout.NORTH);
        centerTopWest.add(sPtAAttributs, BorderLayout.CENTER);

        //CenterTopEast
        centerTopEast.setLayout(new BorderLayout());
        tAMethodes.setEditable(false);                                     //les fenêtres de text ne sont pas editables
        centerTopEast.add(labMethodes, BorderLayout.NORTH);
        centerTopEast.add(sPtAMethodes, BorderLayout.CENTER);

        //CenterTop
        centerTop.setLayout(borderLayoutCenterTop);
        centerTop.add(centerTopWest, BorderLayout.WEST);
        centerTop.add(centerTopEast, BorderLayout.CENTER);

        //CenterMiddleWest
        centerMiddleWest.setLayout(new BorderLayout());
        tASousClasses.setEditable(false);                                  //les fenêtres de text ne sont pas editables
        centerMiddleWest.add(labSousClasses, BorderLayout.NORTH);
        centerMiddleWest.add(sPtASousClasses, BorderLayout.CENTER);

        //CenterMiddleWest
        centerMiddleEast.setLayout(new BorderLayout());
        jLAssAgr.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);     //selection d'un item de la liste à la fois
        centerMiddleEast.add(labAssAgr, BorderLayout.NORTH);
        centerMiddleEast.add(sPjLAssAgr, BorderLayout.CENTER);

        //CenterMiddle
        centerMiddle.setLayout(borderLayoutCenterMiddle);
        centerMiddle.add(centerMiddleWest, BorderLayout.WEST);
        centerMiddle.add(centerMiddleEast, BorderLayout.CENTER);

        //CenterBottom
        centerBottom.setLayout(new BorderLayout());
        tADetails.setEditable(false);                               //les fenêtres de text ne sont pas editables
        centerBottom.add(labDetails, BorderLayout.NORTH);
        centerBottom.add(sPtADetails, BorderLayout.CENTER);

        //Center
        center.setLayout(borderLayoutCenter);
        center.add(centerTop, BorderLayout.NORTH);
        center.add(centerMiddle, BorderLayout.CENTER);
        center.add(centerBottom, BorderLayout.SOUTH);
        principal.add(center, BorderLayout.CENTER);

        add(principal);

        setVisible(true);

        //Écouteur du button "Charger fichier" pour l'ouverture d'un fichier à parser
        butChargerFichier.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser directory = new JFileChooser();
                directory.setCurrentDirectory(new File("."));
                directory.showOpenDialog(principal); //open dialog
                File fileF = directory.getSelectedFile();
                tFChargerFichier.setText(fileF.getName());  //afficher le nom du fichier dans le JTextField
                afficheurChargerFile(fileF.getPath());      //appeller la méthode qui va gérer l'ouverture et le parsing
            }                                                       //du fichier .UCD
        });

        //Écouteur des cliques sur les items du JList des Classes
        jLClasses.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent event) {

                if (!event.getValueIsAdjusting()) {

                    JList source = (JList) event.getSource();
                    int indexClasse = source.getSelectedIndex();            //index de l'item selectionné

                    jLClasses.setSelectedIndex(indexClasse);                //montre l'item selectionné
                    ArrayList<Classe> classeList = model.getClasseList();   //cherche la classé en fonction de l'item choisi
                    classeActive = classeList.get(indexClasse);             //dans la liste et défini la nouvelle classe active
                    afficheurClasse(classeActive);                          //affiche les données de la classe selectionnée
                }
            }
        });

        //Écouteur des cliques sur les items du JList des Associations/Agrégations
        jLAssAgr.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent event) {
                if (!event.getValueIsAdjusting()) {

                    JList source = (JList) event.getSource();
                    int indexClasse = source.getSelectedIndex();

                    jLAssAgr.setSelectedIndex(indexClasse);                             //index de l'item selectionné
                    ArrayList<Link> linkList = model.findClasseLink(classeActive);      //recupère les listes des relations

                    if(!linkList.isEmpty()) {                                           //s'il y a au moins une relation

                        /** Petit bug avec jlist.setSelectedIndex() **/
                        /** des resultat -1 quand la liste des relations n'est pas vide **/
                        /** corrigé avec la condition ci dessous **/
                        if (indexClasse < 0) {                                          //vérifie l'indice de selection

                            tADetails.setText(linkList.get(0).toString());              //affiche la relation
                        } else {
                            tADetails.setText(linkList.get(indexClasse).toString());    //affiche la relation
                        }
                    }
                }
            }
        });

    }

    /**
     * Méthode qui fait le appel à la fonction de parsing et d'affichage des paramètres des classes
     * @param file : path du file
     */
    private void afficheurChargerFile(String file) {

        model = Parser.parse(file);

        // Affichage fenêtre Classes
        DefaultListModel listeModelClasses = new DefaultListModel();
        ArrayList<Classe> classeList = model.getClasseList();
        for (Classe classe : classeList) {
            listeModelClasses.addElement(classe.getId().toString());
        }
        jLClasses.setModel(listeModelClasses);

        // Si le fichier charge possède au moins une classe
        if (!classeList.isEmpty()) {
            classeActive = classeList.get(0);
            afficheurClasse(classeActive);
        }

        jLClasses.setSelectedIndex(0);

    }

    /**
     * Méthode appelé lors de la selection d'une nouvelle classe dans la fenêtre Classes (JList)
     * @param classe : classe à être affiché
     */
    private void afficheurClasse(Classe classe) {

        // Affichage fenêtre Attributs
        ArrayList<String[]> attributList = classe.getAttributList();
        String attributsS = "";
        for (String[] elementAttr : attributList) {
            attributsS += elementAttr[1].toString() + " " + elementAttr[0].toString() + "\n";
        }
        tAAttributs.setText(attributsS);    //affiche les attributs existants (rien s'inexistants)


        // Affichage fenêtre Méthodes
        ArrayList<String[]> methodesList = classe.getOperationsList();
        String methodeS = "";
        for (String[] elementMeth : methodesList) {
            if(elementMeth[1].equals("void")) {
                methodeS += elementMeth[0].toString() + "\n";
            } else {
                methodeS += elementMeth[1].toString() + " " + elementMeth[0].toString() + "\n";
            }

        }
        tAMethodes.setText(methodeS);       //affiche les méthodes existantes (rien s'inexistantes)

        // Affichage fenêtre Sous-classes
        Generalization gener = model.findClasseGeneralization(classeActive);
        if(gener != null) {
            ArrayList<String> generList = gener.getSubclasses();
            String generS = "";
            for (String elementGener : generList) {
                generS += elementGener.toString() + "\n";
            }
            tASousClasses.setText(generS);  //affiche les sous-classes existantes
        } else {
            tASousClasses.setText("");      //n'affiche rien s'il n'y a pas de sous-classes
        }

        // Affichage fenêtres Associations/Agrégations et Détails
        DefaultListModel listeModelAssAgr = new DefaultListModel();
        ArrayList<Link> linkList = model.findClasseLink(classeActive);

        if(!linkList.isEmpty()) {                       // s'il y a des Associations et/ou Agrégations
            for (Link link : linkList) {
                listeModelAssAgr.addElement(link.toStringRelative(classeActive));
            }
            jLAssAgr.setModel(listeModelAssAgr);            // affiche la liste des Associations et/ou Agrégations
            tADetails.setText(linkList.get(0).toString());  // affiche les détails correspondants
            jLAssAgr.setSelectedIndex(0);

        } else {                                        // s'il n'y a pas des Associations ou Agrégations
            listeModelAssAgr.clear();                   // vide la liste des Associations et/ou Agrégations
            tADetails.setText("");                      // vide la fenêtre de Détails
            jLAssAgr.setModel(listeModelAssAgr);
        }


    }
}
