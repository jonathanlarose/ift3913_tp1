import java.util.ArrayList;

/**
 * Développé par:
 * Augusto dos Santos Latgé 20083794
 * Jonathan Larose 20066082.
 */

public class Model {

    //Attributs
    private String id;
    private ArrayList<Classe> classeList;
    private ArrayList<Generalization> generalizationList;
    private ArrayList<Relation> relationList;
    private ArrayList<Aggregation> aggregationList;

    //Getters
    public String getId(){return this.id;}
    public ArrayList<Classe> getClasseList(){return this.classeList;}

    //Constructeur
    public Model(String id){

        this.id = id;
        this.classeList = new ArrayList<Classe>(0);
        this.generalizationList = new ArrayList<Generalization>(0);
        this.relationList = new ArrayList<Relation>(0);
        this.aggregationList = new ArrayList<Aggregation>(0);
    }

    /**
     * Fonction pour ajouter une classe à la liste de classe
     * @param c Classe à ajouter
     */
    public void add(Classe c){
        this.classeList.add(c);
    }

    /**
     * Fonction pour Generalization une classe à la liste de Generalization
     * @param g Generalization à ajouter
     */
    public void add(Generalization g){
        this.generalizationList.add(g);
    }

    /**
     * Fonction pour ajouter une Relation à la liste de Relation
     * @param r Relation à ajouter
     */
    public void add(Relation r){
        this.relationList.add(r);
    }

    /**
     * Fonction pour ajouter une Aggregation à la liste de Aggregation
     * @param a Aggregation à ajouter
     */
    public void add(Aggregation a){
        this.aggregationList.add(a);
    }

    /**
     * Fonction qui retourne un string représentant les détails Relation / Aggregation
     * @return String détails de la Relation / Aggregation
     */
    public String toString(){

        String str = "MODEL : " + id +"\n\n";

        str += "CLASSES :\n";
        for (Classe c : classeList) {
            str += c + "\n";
        }

        str += "\nRELATIONS :\n";
        for(Relation r : relationList)
            str += r + "\n";

        str += "\nAGGREGATIONS :\n";
        for(Aggregation a : aggregationList)
            str += a + "\n";

        return str;
    }

    /**
     * Fonction qui cherche une generalization de la Classe passé en paramètre si elle existe, null sinon.
     * @param c Classe dont on cherche une généralization
     * @return Generalization de la classe passé en paramètre
     */
    public Generalization findClasseGeneralization(Classe c){

        Generalization g = null;

        for(Generalization s : this.generalizationList){
            if(s.getId().equals(c.getId())){
                g = s;
                break;
            }
        }

        return g;
    }

    /**
     * Fonction qui retourne une liste de toute les link (Relation / Aggregation) dont la classe passé en argument fait partie
     * @param c Classe dont ont veut la liste des link dont elle fait partie
     * @return Liste de Link
     */
    public ArrayList<Link> findClasseLink(Classe c){

        ArrayList<Link> linkList = new ArrayList<Link>(0);

        for(Relation r : this.relationList){

            if(r.isClasseInLink(c))
                linkList.add(r);
        }

        for(Aggregation a : this.aggregationList){

            if(a.isClasseInLink(c))
                linkList.add(a);
        }

        return linkList;
    }
}
