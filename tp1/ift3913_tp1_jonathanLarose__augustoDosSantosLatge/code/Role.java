/**
 * Développé par:
 * Augusto dos Santos Latgé 20083794
 * Jonathan Larose 20066082.
 */

public class Role {

    //Attributs
    private String id;
    private Multiplicity multiplicity;

    //Getters
    public String getId() {
        return id;
    }
    public Multiplicity getMultiplicity() {
        return multiplicity;
    }

    //Constructeur
    public Role(String id, Multiplicity multiplicity){

        this.id = id;
        this.multiplicity = multiplicity;
    }

    /**
     * Fonction pour l'affichage
     * @return
     */
    public String toString(){

        return id + " " + multiplicity;
    }
}
