/**
 * Created by Jonathan on 2017-09-27.
 */

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.Buffer;
import java.util.ArrayList;

public class Parser {

    /**
     * Fonction qui parse un fichier de diagramme UML et retourne un object Model pour pouvoir l'afficher
     * @param pathFile Path vers le fichier .ucd à parser
     * @return Model pour l'affichage
     */
    public static Model parse(String pathFile){

        //Model qui contiendra la liste des classes, relations, etc.
        Model model = null;

        try{
            //Initialisation du bufferReader et du model
            BufferedReader br = new BufferedReader(new FileReader(pathFile));
            String line = br.readLine();
            if(line==null)
                throw new Exception("Le fichier est vide.");

            model = new Model(line.substring(6));


            while((line = br.readLine()) != null){

                if(line.equals("") || line.equals(";"))
                    continue;

                String[] lineTab = line.split(" ");

                //Un Model contient des CLASS, des GENERALIZATION, des RELATIONS et des AGGREGATIONS
                //On vérifie de quel type est le prochain bloc à parser et on appel la fonction en conséquence
                switch(lineTab[0]){

                    case "CLASS":
                        String name = (lineTab.length > 1)? lineTab[1] : "unnamed";
                        Classe c = parseClasse(name, br);
                        model.add(c);
                        break;
                    case "GENERALIZATION":
                        if(lineTab.length < 2)
                            throw new Exception("Generalization n'a pas de nom.");
                        Classe cp = model.findClasse(lineTab[1]);
                        Generalization g = parseGeneralization(cp, br, model);
                        model.add(g);
                        break;
                    case "RELATION":
                        if(lineTab.length < 2)
                            throw new Exception("Relation n'a pas de nom.");
                        Relation r = parseRelation(lineTab[1], br, model);
                        model.add(r);
                        break;
                    case "AGGREGATION":
                        Aggregation a = parseAggregation(br, model);
                        model.add(a);
                        break;
                    default:

                }
            }
        } catch (Exception e){
            System.out.println(e);
        }

        return model;
    }

    /**
     * Fonction qui parse une CLASSE et retourne un objet Classe
     * @param id Identificateur de la classe qui sera créé
     * @param br BufferReader pour lire les lignes du fichier
     * @return Un object de type Classe
     */
    private static Classe parseClasse(String id, BufferedReader br){

        Classe c = new Classe(id);

        try {
            String line = br.readLine().replaceAll("\t","").replaceAll("\r","");
            String [] lineTab;

            while (!line.equals(";")){

                //Parse de la liste d'attributs
                lineTab = line.split(" ");

                if(lineTab[0].equals("ATTRIBUTES")) {
                    line = br.readLine();
                    while (!line.startsWith("OPERATIONS")) {

                        line = line.replaceAll(" ", "").replaceAll("\t", "").replaceAll(",", "");

                        String[] att = line.split(":");

                        //Ajoute l'attribut à la liste d'attribut de la Classe
                        c.addAtt(att);
                        line = br.readLine().replaceAll("\t","").replaceAll("\r","");
                    }
                }
                else
                    throw new Exception("Une classe doit contenir une liste d'ATTRIBUTES.");

                //Parse de la liste d'opérations
                lineTab = line.split(" ");

                if(lineTab[0].equals("OPERATIONS")){
                    line = br.readLine().replaceAll(" ", "").replaceAll("\t","").replaceAll("\r","");
                    while(!line.startsWith(";")){

                        int argDeb = line.indexOf("(");
                        int argFin = line.indexOf(")");

                        //Sépare id, args et type
                        String opId = line.substring(0, argDeb);
                        String args = line.substring(argDeb+1, argFin);
                        String type = line.substring(argFin+1).replaceAll(",", "").replaceAll(":","");

                        //Crée un objet Opération et ajoute les différents arguments
                        Operation op = new Operation(opId, type);

                        if(args.length() > 0){
                            String[] argTab = args.split(",");

                            for(String a : argTab){
                                String[] argument = a.split(":");
                                op.addArgument(argument[0],argument[1]);
                            }
                        }

                        //Ajoute l'opération à la liste d'opérations de la Classe
                        c.addOp(op);
                        line = br.readLine().replaceAll(" ", "").replaceAll("\t","").replaceAll("\r","");
                    }
                }
                else
                    throw new Exception("Une classe doit contenir une liste d'OPERATIONS.");
            }

        }catch(Exception e){
            //TODO
            System.out.println(e);
        }

        return c;
    }

    /**
     * Fonction qui parse une GENERALIZATION et retourne un objet Generalization
     * @param parent Identificateur de la Generalization qui sera créé
     * @param br BufferReader pour lire les lignes du fichier
     * @return Une Generalization.
     */
    private static Generalization parseGeneralization(Classe parent, BufferedReader br, Model m){

        Generalization g = new Generalization(parent);

        try{
            String line = br.readLine();
            line = line.replaceAll(",", "");
            String[] lineTab = line.split(" ");

            //Boucle pour ajouter tous les sous-classes
            for(int i=1; i<lineTab.length; i++) {
                Classe sc = m.findClasse(lineTab[i]);

                if(sc != null)
                    g.addSubclass(sc);
            }

        }catch(Exception e){
            System.out.println(e);
        }

        return g;
    }

    /**
     * Fonction qui parse une RELATION et retourne un objet Relation
     * @param id Identificateur de la Relation qui sera créé
     * @param br BufferReader pour lire les lignes du fichier
     * @return Une Relation.
     */
    private static Relation parseRelation(String id, BufferedReader br, Model m){

        Relation r = new Relation(id);

        try{
            String line = br.readLine();
            if(!line.contains("ROLES"))
                throw new Exception("Une relation doit avoir 2 roles.");

            //Parse les deux lignes devant contenir chacun un role et création des Roles
            //Premier Role
            line = br.readLine();
            line = line.substring(line.indexOf("CLASS"), line.length()-1);
            String[] lineTab = line.split(" ");

            //Trouve la classe associé au id
            Classe c = m.findClasse(lineTab[1]);

            Role role = new Role(c, stringToMultiplicity(lineTab[2]));
            r.setRole1(role);

            //Deuxième Role
            line = br.readLine();
            line = line.substring(line.indexOf("CLASS"), line.length());
            lineTab = line.split(" ");

            c = m.findClasse(lineTab[1]);

            role = new Role(c, stringToMultiplicity(lineTab[2]));
            r.setRole2(role);

        }catch (Exception e){
            System.out.println(e);
        }

        return r;
    }

    /**
     * Fonction qui parse une AGGREGATION et retourne un objet Aggregation
     * @param br BufferReader pour lire les lignes du fichier
     * @return Une Aggregation
     */
    private static Aggregation parseAggregation(BufferedReader br, Model m){

        Aggregation a = null;

        try{
            //Parse la ligne du role container et ensuite les différents roles Parts
            //Role Container
            String line = br.readLine();
            if(!line.contains("CONTAINER"))
                throw new Exception("Une relation doit avoir 1 Container.");

            line = br.readLine();
            line = line.substring(line.indexOf("CLASS"));
            String[] lineTab = line.split(" ");

            Classe c = m.findClasse(lineTab[1]);

            Role role = new Role(c, stringToMultiplicity(lineTab[2]));
            a = new Aggregation(role);


            //Roles Parts
            line = br.readLine();
            if(!line.contains("PARTS"))
                throw new Exception("Une relation doit avoir des Parts.");

            line = br.readLine().replaceAll("\t","").replaceAll("\r","");

            while(!line.equals(";")){

                line = line.substring(line.indexOf("CLASS"));
                line = line.replaceAll(",", "");
                lineTab = line.split(" ");

                //S'assure qu'un string sera bien passé à la fonction stringToMultiplicity en cas de UNDEFINED
                String mult = (lineTab.length > 2)? lineTab[2] : "";

                c = m.findClasse(lineTab[1]);

                role = new Role(c, stringToMultiplicity(mult));
                a.addRole(role);

                line = br.readLine().replaceAll("\t","").replaceAll("\r","");
            }

        }catch (Exception e){
            System.out.println(e);
        }

        return a;
    }

    /**
     * Fonction qui convertie un String une Multiplicity enum
     * @param str String qui doit être convertie
     * @return Multiplicity correspondant au String venant du parse
     */
    private static Multiplicity stringToMultiplicity(String str){

        Multiplicity m;

        switch(str){
            case "ONE":
                m = Multiplicity.ONE;
                break;
            case "ONE_OR_MANY":
                m = Multiplicity.ONE_OR_MANY;
                break;
            case "MANY":
                m = Multiplicity.MANY;
                break;
            case "OPTIONALLY_ONE":
                m = Multiplicity.ONE_OR_MANY;
                break;
            default:
                m = Multiplicity.UNDEFINED;
        }

        return m;
    }
}