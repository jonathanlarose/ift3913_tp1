import java.util.ArrayList;

/**
 * Created by Jonathan on 2017-10-30.
 */
public class Metriques {

    /**
     * Fonction qui retourne le nombre moyen d’arguments des méthodes locales pour la classe c.
     * @param c
     * @return moyenne d'arguments des méthodes locales
     */
    public static double ana(Classe c){

        ArrayList<Operation> ops = c.getOperationsList();

        if(ops.isEmpty())
            return 0.;

        double sumArg = 0.;
        double sumOp = 0.;

        //Somme des
        for(Operation o : ops){
            sumOp++;
            sumArg += o.getArgTypeList().size();
        }

        return sumArg/sumOp;
    }

    /**
     * Fonction qui retourne le nombre de méthodes locales/héritées de la classe c. Dans le cas où une méthode
     * est héritée et redéfinie localement (même nom, même ordre et types des arguments et même type de retour),
     * elle ne compte qu’une fois
     * @param c
     * @return nombre de méthodes locales/héritées de la classe c
     */
    public static int nom(Model m, Classe c){

        ArrayList<Operation> methodes = new ArrayList<Operation>(0);
        methodes.addAll(c.getOperationsList()); //methodes locale

        ArrayList<Classe> parents = m.findParentsAll(c);

        //Pour toutes les classes parentes à la classe
        for(Classe p : parents){

            //Vérifie chaque opération
            ArrayList<Operation> ops = p.getOperationsList();

            for(Operation o : ops){

                boolean redef = false;

                //Vérifie si la l'opération n'est pas redéfinir dans les méthodes locales
                for(Operation oLocal : methodes){
                    if(o.equals(oLocal)){
                        redef = true;
                        break;
                    }
                }

                //Si non, on l'ajoute en tant que méthode hérité
                if(!redef)
                    methodes.add(o);
            }
        }

        return methodes.size();
    }

    /**
     * Fonction qui retourne le nombre d’attributs locaux/hérités de la classe c
     * @param c
     * @return nombre d’attributs locaux/hérités de la classe c
     */
    public static int noa(Model m, Classe c){

        int nbAtt = c.getAttributList().size();  //attributs locale
        ArrayList<Classe> parents = m.findParentsAll(c);

        //Pour toutes les classes parentes à la classe
        for(Classe p : parents) {
            nbAtt += p.getAttributList().size();
        }

        return nbAtt;
    }

    /**
     * Fonction qui retourne le nombre de fois où d’autres classes du diagramme apparaissent comme types des
     * arguments des méthodes de c
     * @param m
     * @param c
     * @return nombre de fois où d’autres classes du diagramme apparaissent comme types des
     *              arguments des méthodes de c
     */
    public static int itc(Model m, Classe c){
        int comptClassArg = 0;
        ArrayList<Classe> listeClasses = m.getClasseList();
        ArrayList<Operation> listeOperations = c.getOperationsList();

        //Vérifier pour toutes les classes du model si elles apparessent dans les attributs des opérations de c
        for (Classe classe : listeClasses) {

            //Pour toutes opérations de c
            for (Operation operation : listeOperations) {
                ArrayList<String[]> listeArgument = operation.getArguments();

                // Tester chaque pair type attribut / nom pour chaque argument des opérations
                for (String[] stringTab : listeArgument) {

                    //Vérifier si le nom de la classe du model apparait dans la liste d'arguments
                    for (String string : stringTab) {
                        if (classe.toString().equals(string)){
                            comptClassArg++;
                        }
                    }
                }
            }
        }

        return comptClassArg;
    }

    /**
     * Fonction qui retourne le nombre de fois où c apparaît comme type des arguments dans les méthodes
     * des autres classes du diagramme
     * @param c
     * @return nombre de fois où c apparaît comme type des arguments dans les méthodes des autres
     *              classes du diagramme
     */
    public static int etc(Model m, Classe c){

        int nb = 0; //Compte du nombre de fois que c apparait dans les autres méthodes
        String type = c.getId();

        ArrayList<Classe> classes = m.getClasseList();

        for(Classe mc : classes){

            if(mc.equals(c))
                continue;

            ArrayList<Operation> ops = mc.getOperationsList();

            for(Operation o : ops){

                ArrayList<String> argTypes = o.getArgTypeList();

                for(String a : argTypes)
                    nb += (a.equals(type))? 1 : 0;
            }

        }

        return nb;
    }

    /**
     * Fonction qui retourne le nombre d’associations (incluant les agrégations) locales/héritées auxquelles
     * participe une classe c
     * @param m,c
     * @return nombre d’associations locales/héritées auxquelles participe c
     */
    public static int cac(Model m, Classe c){

        ArrayList<Link> links = m.findClasseLink(c);
        ArrayList<Classe> parents = m.findParentsAll(c);

        for(Classe p : parents){

            ArrayList<Link> plink = m.findClasseLink(p);

            for(Link l : plink){
                if(!links.contains(l))
                    links.add(l);
            }
        }

        return links.size();
    }

    /**
     * Fonction qui retourne la taille du chemin le plus long reliant une classe c à une classe racine dans
     * le graphe d’héritage
     * @param c
     * @return taille du chemin le plus long reliant une classe c à une classe racine dans le graphe d’héritage
     */
    public static int dit(Model m, Classe c){

        int tailleChemin = 0;

        //Parent direct de c
        Classe p = m.findParent(c);

        //Si la classe n'a pas de parents, le chemin est de taille 0
        if(p == null){
            return tailleChemin;
        }
        //Sinon on continue vers la racine
        else{
            return 1 + dit(m,p);
        }
    }

    /**
     * Fonction qui retourne la taille du chemin le plus long reliant une classe c à une classe feuille
     * dans le graphe d’héritage
     * @param c
     * @return taille du chemin le plus long reliant une classe c à une classe feuille
     *              dans le graphe d’héritage
     */
    public static int cld(Model m, Classe c){

        int tailleChemin = 0;

        //Enfants direct de c
        ArrayList<Classe> childList = m.findChildDirect(c);

        //Si la classe n'a pas d'enfants, le chemin est de taille 0
        if (childList.isEmpty()) {
            return tailleChemin;
        }
        //Sinon on cherche le chemin de taille max vers une feuille
        else {
            //On boucle les enfants de c pour trouver celui qui a un chemin de taille max vers une feuille
            for(Classe child : childList) {
                int childChemin = 1+cld(m, child);
                tailleChemin = (tailleChemin > childChemin)? tailleChemin : childChemin;
            }

            return tailleChemin;
        }
    }

    /**
     * Fonction qui retourne le nombre de sous-classes directes de c
     * @param m
     * @param c
     * @return nombre de sous-classes directes de c
     */
    public static int noc(Model m, Classe c){
        ArrayList<Classe> listeSousClassesDirectes = m.findChildDirect(c);

        if (!listeSousClassesDirectes.isEmpty()) {
            return listeSousClassesDirectes.size();
        } else {
            return 0;
        }
    }

    /**
     * Fonction auxilière de nod (récursive)
     * @param m
     * @param c
     * @return nombre de sous-classes
     */
    public static int findSousClasses(Model m, Classe c) {
        ArrayList<Classe> listeSousClasses = m.findChildDirect(c);
        int comptSousClasses = 0;

        if (listeSousClasses.isEmpty()) {
            return comptSousClasses;
        } else {
            for(Classe classe : listeSousClasses) {
                comptSousClasses += 1+findSousClasses(m, classe);
            }
            return comptSousClasses;
        }
    }

    /**
     * Fonction qui retourne le nombre de sous-classes directes et indirectes de c
     * @param m
     * @param c
     * @return nombre de sous-classes directes et indirectes de c
     */
    public static int nod(Model m, Classe c){
        return findSousClasses(m,c);
    }

    /**
     * Fonction qui calcul toutes les métriques pour chaque classe et retourne un string représentant une matrice,  prêt à imprimer pour le CSV
     * @param m
     * @return String représant la matrice de métrique du model
     */
    public static String calculMatriceCSV(Model m){

        String matrice = " ,ANA,NOM,NOA,ITC,ETC,CAC,DIT,CLD,NOC,NOD\r\n";

        for(Classe c : m.getClasseList()){

            matrice += c.getId() + ",";
            matrice += (double) Math.round(Metriques.ana(c)*100)/100 + ",";
            matrice += nom(m,c) + ",";
            matrice += noa(m,c) + ",";
            matrice += itc(m,c) + ",";
            matrice += etc(m,c) + ",";
            matrice += cac(m,c) + ",";
            matrice += dit(m,c) + ",";
            matrice += cld(m,c) + ",";
            matrice += noc(m,c) + ",";
            matrice += nod(m,c) + "\r\n";
        }

        return matrice;
    }

    /**
     * Fonction qui retourne la définition de la métrique demandé.
     * @param metriqueAbreviation
     * @return String définition de la métrique
     */
    public static String definition(String metriqueAbreviation){

        String def = "";

        switch(metriqueAbreviation){

            case "ANA":
                def = "ANA(x) : Nombre moyen d’arguments des méthodes locales pour la classe x.";
                break;
            case "NOM":
                def = "NOM(x) : Nombre de méthodes locales/héritées de la classe x.";
                break;
            case "NOA":
                def = "NOA(x) : Nombre d’attributs locaux/hérités de la classe x.";
                break;
            case "ITC":
                def = "ITC(x) : Nombre de fois où d’autres classes du diagramme apparaissent comme types des \narguments des méthodes de x.";
                break;
            case "ETC":
                def = "ETC(x) : Nombre de fois où x apparaît comme type des arguments dans les méthodes des \nautres classes du diagramme.";
                break;
            case "CAC":
                def = "CAC(x) : Nombre d’associations (incluant les agrégations) locales/héritées auxquelles \nparticipe une classe x.";
                break;
            case "DIT":
                def = "DIT(x) : Taille du chemin le plus long reliant une classe x à une classe racine dans le graphe \nd’héritage.";
                break;
            case "CLD":
                def = "CLD(x) : Taille du chemin le plus long reliant une classe x à une classe feuille dans le graphe \nd’héritage.";
                break;
            case "NOC":
                def = "NOC(x) : Nombre de sous-classes directes de x.";
                break;
            case "NOD":
                def = "NOD(x) : Nombre de sous-classes directes et indirectes de x.";
                break;

        }

        return def;
    }
}
