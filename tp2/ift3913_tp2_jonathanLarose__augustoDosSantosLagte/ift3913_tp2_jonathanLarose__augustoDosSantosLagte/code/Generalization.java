import java.util.ArrayList;

/**
 * Développé par:
 * Augusto dos Santos Latgé 20083794
 * Jonathan Larose 20066082.
 */

public class Generalization {

    //Attributs
    private Classe parent;
    private ArrayList<Classe> subclasses;

    //Getters
    public Classe getParent() {
        return parent;
    }
    public ArrayList<Classe> getSubclasses() {
        return subclasses;
    }

    public Generalization(Classe parent){
        this.parent = parent;
        this.subclasses = new ArrayList<Classe>(0);
    }

    /**
     * Fonction qui ajoute une sous-classes à la liste de sous-classes
     * @param subclass String Identificateur de la sous-classes à ajouter
     */
    public void addSubclass(Classe subclass){
        this.subclasses.add(subclass);
    }
}
