/**
 * Développé par:
 * Augusto dos Santos Latgé 20083794
 * Jonathan Larose 20066082.
 */

public interface Link {

    /**
     * Fonction qui retourne un string représentant la Relation / Aggregation en fonction de la classe passé en argument
     * @param c Classe
     * @return String de la Relation / Aggregation relativement à la classe
     */
    String toStringRelative(Classe c);

    /**
     * Fonction qui vérifie si la classe passé en argument fait partie de la Relation / Aggregation
     * @param c Classe qu'on veut vérifier
     * @return True si oui, false si non
     */
    boolean isClasseInLink(Classe c);

    /**
     * Fonction qui retourne un string représentant les détails Relation / Aggregation
     * @return String détails de la Relation / Aggregation
     */
    String toString();
}
